# Host configuration

- Wiring to Odroid C2:

    Red -> pin 1
    Blue -> pin 7
    Yellow -> pin 19
    Green -> pin 33

- Typical user stuff (pretty much same as server):

    apt-get update
    apt-get dist-upgrade
    apt-get install iputils-ping ssh sudo vim git zsh emacs-nox tmux # some of this was already installed
    addgroup --gid 500 <user>
    adduser --uid 500 --gid 500 <user>
    addgroup <user> sudo

Needed to create home directory as adduser didn't do that

    dpkg-reconfigure tzdata

    sudo dpkg-reconfigure locales # get rid of perl warning about locales

- getting pwm working

    sudo modprobe pwm-meson npwm=2
    sudo modprobe pwm-ctrl

- enable pwm at boot / pwm should be for fan and servo

add to /et/modules:

    pwm-meson
    pwm-ctrl

- create /etc/modprobe.d/a_name.conf

    options pwm-meson npwm=2

- Enable 1 wire bus / for temp sensors / chip: MAX31850 / https://www.adafruit.com/product/1727

    # modprobe w1-gpio && modprobe w1-therm
    # cd /sys/bus/w1/devices/
    # ls
    3b-0c98073da856  3b-2c98073db958  w1_bus_master1
    # cd 3b-0c98073da856
    # cat w1_slave
    70 01 20 1d f0 ff ff ff b9 : crc=b9 YES
    70 01 20 1d f0 ff ff ff b9 t=23000

- Add to /etc/modules:

    w1-gpio
    w1-thermw1-therm

- Networking

ubuntu 20 uses netplan for neowrking
create /etc/netplan/01-all.yaml

```yaml
network:
    version: 2
    renderer: networkd
    ethernets:
        enp3s0:
            dhcp4: true
            optional: true
    wifis:
        wlan0:
            dhcp4: true
            access-points:
                "ap name goes here":
                    password: "password goes here"
            optional: true
```

- Disable NetworkManager

    systemctl disable NetworkManager.service # not sure if this is needed

    netplan apply

    networkctl list
    IDX LINK  TYPE     OPERATIONAL SETUP     
      1 lo    loopback carrier     unmanaged 
      2 eth0  ether    routable    unmanaged 
      3 wlan0 wlan     routable    configured
