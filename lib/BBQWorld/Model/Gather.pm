package BBQWorld::Model::Gather;

use strict;
use warnings;

#use Mojo::Log;
#use FindBin;

use BBQWorld::Model::PID;
use BBQWorld::Model::Temps;

#use BBQWorld::Model::AirIntake;
use BBQWorld::Model::AirDevice;

use Data::Dumper;

use Mojo::IOLoop;

sub new {
    my ( $class, $config, $log ) = @_;

    my $self = bless { output => 0 }, $class;

    $self->{temps} = BBQWorld::Model::Temps->new( $config->{probes} );

    $self->{log} = $log;

    #$self->{log} = Mojo::Log->new(path => "$FindBin::Bin/../log/bbqworld.log");

    #    Mojo::IOLoop->recurring(
    #        1 => sub {
    #            $self->{temps}->cache_temps();
    #        }
    #    );

    my $get_temps  = sub { return $self->{temps}->get_temps };
    my $get_output = sub { return $self->{output} };
    $self->{pid} = BBQWorld::Model::PID->new( $config->{pid},
					      \&$get_temps,
	                                      \&$get_output);

    #my $temps = $self->{temps}->get_temps;   # prime the pump

    $self->{pid}->set_mode(1); # turn the PID on
    
    $self->{fan} = BBQWorld::Model::AirDevice->new( 'fan', $config->{air_devices}{fan},
        $self->{log} );
    $self->{valve} =
      BBQWorld::Model::AirDevice->new( 'valve', $config->{air_devices}{valve},
        $self->{log} );

    #print Dumper($config->{air_devices}{fan});
    #    $self->{air_intake} = BBQWorld::Model::AirIntake->new;

    Mojo::IOLoop->recurring(
        $config->{pid}{sampletime} => sub {
            my $res;
            #$res->{temps} = $self->{temps}->get_temps;
            #my $out = $self->{pid}->calc_pid( $res->{temps} );
            my $out = $self->{pid}->calc_pid();
	    if ($out) {
		$res = $out;
	    } else {
		return;   # not time for a sample
	    }

	    if ($res->{pid}{Output} != -1.0) {
      	        $self->{output} = $res->{pid}{Output};
	    }
            $res->{intake}{fan} = $self->{fan}->set_speed( $self->{output} );
            $res->{intake}{valve} =
		$self->{valve}->set_speed( $self->{output} );
	    #$res->{temps} = $temps;
            $self->{stats} = $res;

	    #$self->{log}->info( Dumper($res) );
            #$res->{intakes} = $self->{air_intake}->set_volume( $res->{values}{CO} );
        }
    );

    return $self;
}

sub stats {
    my $self = shift;

    #$self->{log}->debug( Dumper($self->{stats}) );
    return $self->{stats};
}

sub mode {
    my ( $self, $info ) = @_;

    my $mode = $info->{mode};
    $self->{log}->info( "user wants mode: $mode" );

    $self->{pid}->set_mode($mode);

    if ($mode == 0) {
        #$self->{output} = $res->{pid}{Output};
	$self->{output} = $info->{output};
        $self->{fan}->set_speed( $self->{output} );
        $self->{valve}->set_speed( $self->{output} );
    }
}

1;
