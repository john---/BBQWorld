package BBQWorld::Model::PID;

# http://brettbeauregard.com/blog/2011/04/improving-the-beginners-pid-introduction/
# https://github.com/br3ttb/Arduino-PID-Library/blob/master/PID_v1.cpp

use Data::Dumper;
use Carp;

sub new {
    my ( $class, $args, $get_temps, $get_output ) = @_;

    croak "must specify all params"
      unless defined $args->{Kp}
      and defined $args->{Ki}
      and defined $args->{Kd}
      and defined $args->{setpoint}
      and defined $args->{sampletime};

    my $self = {
	get_temps => $get_temps,
	get_output    => $get_output,
        setpoint  => $args->{setpoint},
	in_auto => 0,  # start out in manual mode
	sampletime => $args->{sampletime},
        last_time   => time() - $args->{sampletime},
    };

    $self->{Output} => &{$self->{get_output}};

    bless $self, $class;

    #$self->set_output_limits(0, 1023);  # set to Odroid C2 PWM limits
    $self->set_output_limits(0, 126);  # set to max pwm for valve
    $self->set_tunings($args->{Kp}, $args->{Ki}, $args->{Kd}, $args->{POn});

    return $self;
}

sub calc_pid {
    my ( $self ) = @_;

    my $now        = time();
    my $time_delta = $now - $self->{last_time};
    if ( $time_delta < $self->{sampletime} ) {
        return;
    }

    my $temps = &{$self->{get_temps}};
    $self->{values}{temps} = $temps;

    if (!$self->{in_auto}) {
	my %res = (
	    P => -1.0,
	    I => -1.0,
	    D => -1.0,
	    Output => -1.0,
	    SP => -1.0,
	    );

	$self->{values}{pid} = \%res;

	return $self->{values};
    }

    my $input = $temps->{ambient};
    my $error = $self->{setpoint} - $input;
    my $dInput = $input - $self->{last_input};
    $self->{ITerm} += $self->{gains}{Ki} * $error;   # I
    $self->{ITerm} = $self->restrict( $self->{ITerm} );
#    $self->{output_sum} += $self->{gains}{Ki} * $error;   # I

    # Add Proportional on Measurement, if P_ON_M is specified
    if ($self->{POn} eq 'p_on_m') {
	$self->{PTerm} -= $self->{gains}{Kp} * $dInput;
    } else {    # p_on_e
        $self->{PTerm} = $self->{gains}{Kp} * $error;
    }
    $self->{PTerm} = $self->restrict( $self->{PTerm} );

    $self->{DTerm} = $self->{gains}{Kd} * $dInput;  # TODO: this is not currently resisting
                                                    # movement towards the setpoint

    $self->{Output} = $self->{PTerm} + $self->{ITerm} - $self->{DTerm};

    $self->{Output} = $self->restrict( $self->{Output} );

    #print sprintf( "input:%.0f P:%.0f I:%.0f D:%.0f CO:%.0f\n",
    #    $input, $P, $I, $D, $total );

#    $self->{prev_error} = $error;
    $self->{last_input} = $input;
    $self->{last_time}  = $now;

    my %res = (
        P => $self->{PTerm},
	I => $self->{ITerm},
	D => $self->{DTerm},
        Output => $self->{Output},
        SP => $self->{setpoint},
    );

    $self->{values}{pid} = \%res;
    #$self->{values}{temps} = $temps;

    return $self->{values};
}

sub set_output_limits {
    my ($self, $min, $max) = @_;

    if ($min >= $max) { return; }

    $self->{out_min} = $min;
    $self->{out_max} = $max;

    if ($self->{in_auto}) {
	$self->{Output} = $self->restrict( $self->{Output} );
	#if ($self->{output} > $self->{out_max}) { $self->{output} = $self->{out_max}
	#} elsif ($self->{output} < $self->{out_min}) { $self->{output} = $self->{out_min} }

	$self->{PTerm} = $self->restrict( $self->{PTerm} );
	$self->{ITerm} = $self->restrict( $self->{ITerm} );

	#if ($self->{output_sum} > $self->{out_max}) { $self->{output_sum} = $self->{out_max}
	#} elsif ($self->{output_sum} < $self->{out_min}) { $self->{output_sum} = $self->{out_min} }
    }
}

sub set_tunings {
    my ($self, $Kp, $Ki, $Kd, $POn) = @_;

    if ( ($Kp < 0) || ($Ki < 0) || ($Kd < 0) ) { return; }

    $self->{POn} = $POn;

    $self->{gains}{disp_Kp} = $Kp;
    $self->{gains}{disp_Ki} = $Ki;
    $self->{gains}{disp_Kd} = $Kd;

    $self->{gains}{Kp} = $Kp;
    $self->{gains}{Ki} = $Ki * $self->{sampletime};
    $self->{gains}{Kd} = $Kd / $self->{sampletime};
}

sub set_mode {
    my ($self, $mode) = @_;

    my $new_auto = 0;
    if ($mode == 1) { $new_auto = 1 }

    if ($new_auto && !$self->{in_auto}) {
	$self->initialize();
    }

    $self->{in_auto} = $new_auto;
}

# does all the things that need to happen to ensure a bumpless transfer
# from manual to automatic mode.
sub initialize {
    my ($self, $input) = @_;

    #$self->{output_sum} = $output;
    $self->{ITerm} = &{$self->{get_output}};
    $self->{PTerm} = 0;
    my $temps = &{$self->{get_temps}};
    $self->{last_input} = $temps->{ambient};
    $self->{init_input} = $temps->{ambient};

    $self->restrict( $self->{ITerm} );

    #print sprintf('initializing PID: Iterm: %f PTerm; %f last_input: %f init_input: %f',$self->{ITerm}, $self->{PTerm}, $self->{last_input}, $self->{init_input});
    #if ($self->{output_sum} > $self->{out_max}) { $self->{output_sum} = $self->{out_max}
    #} elsif ($self->{output_sum} < $self->{out_min}) { $self->{output_sum} = $self->{out_min} }  
}

sub restrict {
    my ($self, $amount) = @_;

    if ( $amount > $self->{out_max} ) {
        return $self->{out_max};
    } elsif ( $amount < $self->{out_min} ) {
        return $self->{out_min};
    } else {
	return $amount;
    }
}

sub get_values {
    my $self = shift;

    return $self->{values};
}

sub get_gains {
    my $self = shift;

    return $self->{gains};
}

1;
